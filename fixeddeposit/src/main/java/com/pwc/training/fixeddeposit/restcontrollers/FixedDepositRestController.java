package com.pwc.training.fixeddeposit.restcontrollers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.HashMap;
import java.util.Map;

@RestController
public class FixedDepositRestController {


    @PostMapping("/fixeddeposit")
    public Flux<Map<String, String>> handleCreateFixedDeposit() {


        Map<String, String> response = new HashMap<>();
        response.put("message", "success");

        return Flux.just(response);


    }


}
