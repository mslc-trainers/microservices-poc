package com.pwc.training.customerservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pwc.training.customerservice.model.Customer;
import com.pwc.training.customerservice.model.CustomerAddress;

import java.util.Arrays;
import java.util.List;

public class JacksonDemoReader {


    public static void main(String[] args) throws JsonProcessingException {

        String json = "{\n" +
                "  \"id\" : 101,\n" +
                "  \"name\" : \"IBM\",\n" +
                "  \"address\" : \"Mumbai\",\n" +
                "  \"deliverAddresses\" : [ {\n" +
                "    \"line1\" : \"High Street\",\n" +
                "    \"line2\" : \"Lokhandwala\"\n" +
                "  }, {\n" +
                "    \"line1\" : \"Walkeshwar\",\n" +
                "    \"line2\" : \"High City\"\n" +
                "  } ]\n" +
                "}";

        ObjectMapper mapper = new ObjectMapper();
        Customer customer = mapper.readValue(json, Customer.class);

        System.out.println(customer.getId() + " -- " + customer.getName());




    }
}
